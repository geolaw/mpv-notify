# mpv-notify

fork of mpv-notify - fixing musicbrainz lookup
https://github.com/rohieb/mpv-notify

January 29 2019 - forking rohieb's mpv-notify to provide fix for the
musicbrainz look up.
I am not a lua programmer so this is going to be an exacy clone except for the
one line fix - removing "local" which seems to be restricting the scope of the
mbid variable

mpv-notify install notes : 
mpv appears to use lua 5.2 out of the box.
installing stuff with luarocks wants to install to the lua 5.3 path - so need to specify the lua version to install to 

sudo luarocks  --lua-version 5.2 install xml2lua
sudo luarocks  --lua-version 5.2 install luaposix

